import {Component, OnInit} from '@angular/core';

import { Platform} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import {Router} from '@angular/router';
import {AuthService} from './service/auth.service';
import {UserProfilModel} from './model/user/userProfil.model';
import {UserService} from './service/user.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  userConnectedPage: UserProfilModel;
  public appPages = [
    {
      title: 'Accueil',
      url: '/accueil',
      icon: 'home'
    },
    {
        title: 'Voiture',
        url: '/',
        icon: 'car'
    },
    {
        title: 'Voiture Acheté',
        url: '/car-solded',
        icon: 'cart'
    },
    {
        title: 'Profile',
        url: '/user-profil',
        icon: 'contact'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private userService: UserService,
    private authService: AuthService) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  logoutUser() {
    this.router.navigate(['login']);
    this.authService.logout();
  }

  getIsAuth(): boolean {
      if (this.authService.authenticated()) {
          this.userConnectedPage = this.userService.getUserConnected();
          this.userService.userConnected$.subscribe(
              (userConnected) => {
                  this.userConnectedPage = userConnected;
              });
          return true;
      } else {
          return false;
      }
  }
}
