import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { AuthGuardService } from './service/auth-guard.service';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'car-list',
    pathMatch: 'full'
  },
  { path: 'car-list',
      children: [
          {
              path: '',
              loadChildren: './pages/car-list/car-list.module#CarListPageModule',
          },
          {
              path: ':carId',
              loadChildren: './pages/car-detail/car-detail.module#CarDetailPageModule'
          },
      ]
  },
  {
      path: 'login',
      loadChildren: './pages/login/login.module#LoginPageModule'
  },
  {
    path: 'accueil',
    canActivate: [AuthGuardService],
    loadChildren: './pages/accueil/accueil.module#HomePageModule'
  },
  { path: 'car-add', loadChildren: './pages/car-add/car-add.module#CarAddPageModule' },
  { path: 'car-solded',
    canActivate: [AuthGuardService],
    loadChildren: './pages/car-solded/car-solded.module#CarSoldedPageModule'
  },
  {   path: 'user-profil',
      canActivate: [AuthGuardService],
      loadChildren: './pages/user-profil/user-profil.module#UserProfilPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { path: 'about', loadChildren: './pages/about/about.module#AboutPageModule' },
  { path: '**', loadChildren: './pages/four-zero-four/four-zero-four.module#FourZeroFourPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
