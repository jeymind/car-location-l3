import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ControlService {

  constructor() { }

  generateIdObject(tab: any[], firstString: string): string {
      let numIdInt: number = 0;
      let numIdString: string = '';
      let numCar: string;
      tab.forEach(
          (object) => {
              numIdString = object.id;
          }
      );
      if (numIdString === '') {
          numIdInt = 1;
      } else {
          numIdString = numIdString.replace(firstString, '0');
          numIdInt = +numIdString;
          numIdInt += 1;
      }
      if (numIdInt <= 9) {
          numCar = firstString + '000' + numIdInt;
      } else if (numIdInt <= 99 ) {
          numCar = firstString + '00' + numIdInt;
      } else if ( numIdInt <= 999) {
          numCar = firstString + '0' + numIdInt;
      } else if ( numIdInt <= 9999) {
          numCar = firstString + numIdInt;
      }
      return numCar;
  }

}
