import { Injectable } from '@angular/core';
import {Car} from '../model/car/car.model';
import * as firebase from 'firebase';
import {Subject} from 'rxjs';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireStorage } from 'angularfire2/storage';
import {UserProfilModel} from '../model/user/userProfil.model';
import {finalize} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CarService {
    cars: Car[] = [];
    carsSource = new Subject<Car[]>();
    cars$ = this.carsSource.asObservable();
    private carListRef = this.db.list<Car>('Cars/');
    constructor( private db: AngularFireDatabase,
                 private afStorage: AngularFireStorage) {}
    // Running
    getCarListFire() {
        return this.carListRef.snapshotChanges();
    }
    setAllCarClass(cars: Car[]){
        this.cars = cars;
        this.emitCars();
    }
    getCarsClass(): Car[] {
        return this.cars;
    }
    emitCars() {
        this.carsSource.next(this.cars);
    }
    getSingleCar(carId: string){
        return this.cars.find(
            (carEl) => {
                return carEl.id === carId;
            }
        );
    }
    addCarBase(car: Car) {
        this.cars.push(car);
        this.saveCars();
        this.emitCars();
    }
    updateCarBase(index: string, car: Car) {
        return this.carListRef.update(index, car);
    }
    getCarFileRef(carId: string) {
        return this.afStorage.ref('Car/' + carId + '.jpg');
    }
    findIndexCar(car: Car): number {
        const indexToUpdate = this.cars.findIndex(
            (carEl) => {
                return carEl.id === car.id;
            }
        );
        return indexToUpdate;
    }
    saveStoreCars(file: File, car: Car) {
        let find: boolean = false;
        const indexToUpdate = this.cars.findIndex(
            (carEl) => {
                if (carEl.id === car.id) {
                    find = true;
                    return find;
                }
            }
        );
        if ( file ) {
            const carFileRef = this.getCarFileRef(car.id);
            const uploadFile = this.afStorage.upload('Car/' + car.id + '.jpg', file);
            uploadFile.snapshotChanges().pipe(
                finalize( () => {
                    carFileRef.getDownloadURL().subscribe( downloadUrl => {
                        car.urlImage = downloadUrl;
                        if ( find ) {
                            this.updateCarBase(indexToUpdate.toString(), car).then();
                        } else {
                            this.addCarBase(car);
                        }
                    });
                })
            ).subscribe();
        } else {
            if ( find ) {
                this.updateCarBase(indexToUpdate.toString(), car).then();
            } else {
                car.urlImage = 'https://firebasestorage.googleapis.com/v0/b/car-location-a2da9.appspot.com/o/Car%2Flogo.png?alt=media&token=b5cfcb4c-d871-4711-a6b2-9ad1e49522b4';
                this.addCarBase(car);
            }
        }
    }
    removeCar(carId: string) {
        this.getCarFileRef(carId).delete();
        const carIndexToRemove = this.cars.findIndex(
            carEl => carEl.id === carId
        );
        this.cars.splice(carIndexToRemove, 1);
        this.saveCars();
        this.emitCars();
    }
    saveCars() {
        this.db.database.ref('Cars/').set(this.cars);
    }
}
