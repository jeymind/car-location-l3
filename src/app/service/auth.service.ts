import { Injectable } from '@angular/core';
import { AngularFireAuth} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import AuthProvider = firebase.auth.AuthProvider;
import {UserService} from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private user: firebase.User;
  private isAuth = false;
  private loginConnected: string;
  constructor(private afAuth: AngularFireAuth) {}
  signInWithEmail(client) {
    return this.afAuth.auth.signInWithEmailAndPassword(client.login, client.password);
  }

  signUp(client) {
      return this.afAuth.auth.createUserWithEmailAndPassword(client.login, client.password);
  }

  login(): void {
    this.isAuth = true;
  }
  logout() {
    this.isAuth = false;
  }
  authenticated(): boolean {
      return this.isAuth;
  }

  setLoginConnected(login: string) {
      this.loginConnected = login;
  }
  getLoginConnected(): string {
      return this.loginConnected;
  }
}
