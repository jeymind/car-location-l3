import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireStorage, AngularFireUploadTask} from 'angularfire2/storage';

@Injectable({
  providedIn: 'root'
})
export class UploadFileService {

  constructor(
      private db: AngularFireDatabase,
      private afStorage: AngularFireStorage,
  ) { }
  getFiles(){
    let ref = this.db.list('files');

    return ref.snapshotChanges()
        .subscribe(changes => {
          return changes.map(
              c => ({
                key: c.payload.key, ...c.payload.val()
              }));
        });
  }
  uploadToStorage(information): AngularFireUploadTask {
    let newName = `${new Date().getTime()}.txt`;
    return this.afStorage.ref(`files/${newName}`).putString(information);

  }
  storeInfoToDatabase(metaInfo){
    let toSave = {
      created: metaInfo.timeCreated,
      url: metaInfo.downloadURLs[0],
      fullPath: metaInfo.fullPath,
      contentType: metaInfo.content.contentStyleType
    };
    return this.db.list('files').push(toSave);
  }
  deleteFile(file){
    let key = file.key;
    let storagePath = file.key;

    // Effacer de la base
    this.db.list('files').remove(key);
    // Effacer la fichier dans de base de donnée
    return this.afStorage.ref(storagePath).delete();
  }
}
