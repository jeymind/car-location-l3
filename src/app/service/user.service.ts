import {Injectable} from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { UserProfilModel } from '../model/user/userProfil.model';
import {Subject} from 'rxjs';
import { AngularFireStorage } from 'angularfire2/storage';
import {finalize} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {
    userClass: UserProfilModel[] = [];
    userConnected: UserProfilModel;
    private userConnectedSource = new Subject<UserProfilModel>();
    userConnected$ = this.userConnectedSource.asObservable();
    private userListRef = this.db.list<UserProfilModel>('User/');

    constructor( private db: AngularFireDatabase,
                 private afStorage: AngularFireStorage) { }


    getUserList() {
        return this.userListRef.snapshotChanges();
    }

    emitUserConnected() {
        this.userConnectedSource.next(this.userConnected);
    }

    setUserConnected(login: string) {
        this.userConnected = this.userClass.filter( user => user.login === login)[0];
        this.emitUserConnected();
    }
    getUserConnected() {
        return this.userConnected;
    }

    setAllUserClass(users: UserProfilModel[]) {
        this.userClass = users;
    }

    getUserListOld() {
        return this.userListRef.valueChanges();
    }

    addUser(user: UserProfilModel) {
        return this.userListRef.push(user);
    }
    updateUser(user: UserProfilModel) {
        return this.userListRef.update(user.key, user);
    }
    removeUserBase(user: UserProfilModel) {
        return this.userListRef.remove(user.key);
    }
    removeUserImage(user: UserProfilModel) {
        const storageRef = this.afStorage.ref('/User');
        storageRef.child(user.login + '.jpg').delete();
    }
    deleteUser(user: UserProfilModel){
        this.removeUserBase(user)
            .then(() => {
                this.removeUserImage(user);
            })
            .catch(error => console.log(error));
    }
    getUserFileRef(login) {
        return this.afStorage.ref('User/' + login + '.jpg');
    }
    saveUser(file: File, client: UserProfilModel) {
        if ( file ) {
            const userFileRef = this.getUserFileRef(client.login);
            const uploadFile = this.afStorage.upload('User/' + client.login + '.jpg', file);
            uploadFile.snapshotChanges().pipe(
                finalize( () => {
                    userFileRef.getDownloadURL().subscribe( downloadUrl => {
                        client.urlImage = downloadUrl;
                        if (client.key === '' ) {
                            this.addUser(client).then();
                        } else {
                            this.updateUser(client).then();
                        }
                    });
                })
            ).subscribe();
        } else {
            if (client.key === '' ) {
                client.urlImage = 'https://firebasestorage.googleapis.com/v0/b/car-location-a2da9.appspot.com/o/User%2Fuser.png?alt=media&token=ca1c7d03-ab03-4200-8c70-374be1abfea9';
                this.addUser(client).then();
            } else {
                this.updateUser(client).then();
            }
        }
    }

}
