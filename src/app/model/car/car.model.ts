export class Car {
    id: string;
    type: string;
    marque: string;
    prix: number;
    location: number;
    urlImage: string;
    description: string;
    status: string;
}
