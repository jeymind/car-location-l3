export class UserProfilModel {
    key ?: string;
    login: string;
    name: string;
    firstName ?: string;
    gender: string;
    cin: number;
    location: string;
    password: string;
    urlImage: string;
    typeUser: string;
}
