import { Component, OnInit } from '@angular/core';
import {FormGroup, NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../../service/auth.service';
import {UserService} from '../../service/user.service';
import {UserProfilModel} from '../../model/user/userProfil.model';
import {MenuController} from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginError: string;
  noConn = 'A network error (such as timeout, interrupted connection or unreachable host) has occurred.';
  noEmail = 'There is no user record corresponding to this identifier. The user may have been deleted.';
  wroungPwd = 'The password is invalid or the user does not have a password.';
  disabledAccount = 'The user account has been disabled by an administrator.';
  loading = 'Verification ...';

  passwordIcon = 'eye-off';
  passwordType = 'password';

  constructor(private router: Router,
              private authService: AuthService,
              private userService: UserService,
              public menuCtrl: MenuController) {}

  initialiseClassUserLogin(){
      this.userService.getUserList().subscribe(
          data => {
              let users = data.map(
                  e => {
                      return {
                          key: e.payload.key,
                          login: e.payload.val().login,
                          name: e.payload.val().name,
                          firstName : e.payload.val().firstName,
                          gender: e.payload.val().gender,
                          cin: e.payload.val().cin,
                          location: e.payload.val().location,
                          password: e.payload.val().password,
                          urlImage: e.payload.val().urlImage,
                          typeUser: e.payload.val().typeUser
                      };
                  }
              );
              this.userService.setAllUserClass(users);
          }
      );
  }

  ngOnInit() {
    this.menuCtrl.enable(this.authService.authenticated());
  }

  onSubmitLogin(form: NgForm) {
    this.initialiseClassUserLogin();
    const login = form.value['login'];
    const password = form.value['mdp'];
    let client = {
      login,
      password
    };
    this.loginError = this.loading;
    this.authService.signInWithEmail(client)
        .then(
            () => {
              this.loginError = '';
              form.reset();
              this.authService.login();
              this.router.navigate(['accueil']);
              this.userService.setUserConnected(client.login);
            },
            (error) => {
              this.loginError = error.message;
            }
        );
  }

    showHidePwd() {
      this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
      this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    }

    messageResponse(): string {
      if (this.loginError === this.noConn) {
          return 'Il n\'y a pas de connexion. Veuillez connecter votre appareil';
      } else if ( this.loginError === this.noEmail ){
          return 'Utilisateur non inscrit. Veuillez incrire gratuitement';
      } else if (this.loginError === this.wroungPwd) {
          return 'Mot de passe incorrect';
      } else if (this.loginError === this.disabledAccount) {
            return 'Votre compte a été desactiver. Veuillez Contacter les services';
      } else {
          return '';
      }
    }
}
