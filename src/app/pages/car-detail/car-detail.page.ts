import { Component, OnInit } from '@angular/core';
import {CarService} from '../../service/car.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertController, MenuController} from '@ionic/angular';
import {AuthService} from '../../service/auth.service';
import {Car} from '../../model/car/car.model';
import {NgForm} from '@angular/forms';
import {UserService} from '../../service/user.service';

@Component({
  selector: 'app-car-detail',
  templateUrl: './car-detail.page.html',
  styleUrls: ['./car-detail.page.scss'],
})
export class CarDetailPage implements OnInit {
    action: boolean;
    loadedCar: Car;
    car: Car;
    carId: string;
    file: File;
    imageToUpload: string;
    constructor(private authService: AuthService,
                public menuCtrl: MenuController,
                private activatedRoute: ActivatedRoute,
                private carService: CarService,
                private userService: UserService,
                private router: Router,
                private alertCtrl: AlertController
    ) { }

    ngOnInit() {
        // this.car = new Car();
        this.activatedRoute.paramMap.subscribe(paraMap => {
            if (!paraMap.has('carId')) {
                // redirect
                return;
            }
            this.carId = paraMap.get('carId');
            // const carId = paraMap.get('carId');
            this.loadedCar = this.carService.getSingleCar(this.carId);
            this.imageToUpload = this.loadedCar.urlImage;
        });
        this.action = false;
    }
    getIsAuthAdmin(): boolean {
        if(this.authService.authenticated() && (this.userService.getUserConnected().typeUser === 'admin') ) {
            return true;
        } else {
            return false;
        }
    }
    setEditAction() {
        this.action = true;
    }
    onSubmitUpdateUser(form: NgForm) {
        this.carService.saveStoreCars(this.file, this.loadedCar);
        // this.userService.emitUserConnected();
        this.action = false;
    }
    setCarSold() {
        this.loadedCar.status = this.userService.getUserConnected().login;
        let indexCar = this.carService.findIndexCar(this.loadedCar);
        this.carService.updateCarBase(indexCar.toString(), this.loadedCar).then();
    }
    onDeleteCar() {
        this.alertCtrl.create({ header: 'Etes vous sûr?', message: 'voulez vous vraiment supprimer?',
            buttons: [
                {
                    text: 'Cancel', role: 'cancel'
                },
                {
                    text: 'Delete',
                    handler: () => {
                        this.carService.removeCar(this.carId);
                        this.router.navigate(['/car-list']);
                    }
                }
            ]}).then(alertEl => {
            alertEl.present();
        });
    }
    authentifed(): boolean {
        return this.authService.authenticated();
    }
    changeListener($event) {
        this.file = $event.target.files[0];
        if (this.file !== null) {
            let reader = new FileReader();
            reader.onload = (event: any) => {
                this.imageToUpload = event.target.result;
            };
            reader.readAsDataURL($event.target.files[0]);
        }
    }
}
