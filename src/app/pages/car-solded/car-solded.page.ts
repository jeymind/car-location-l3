import { Component, OnInit } from '@angular/core';
import {CarService} from '../../service/car.service';
import {Car} from '../../model/car/car.model';
import {UserService} from '../../service/user.service';
import {UserProfilModel} from '../../model/user/userProfil.model';

@Component({
  selector: 'app-car-solded',
  templateUrl: './car-solded.page.html',
  styleUrls: ['./car-solded.page.scss'],
})
export class CarSoldedPage implements OnInit {
  loading: boolean = true;
  cars: Car[];
  userConnectedPage: UserProfilModel;

  constructor(private carService: CarService,
              private userService: UserService) { }

  ngOnInit() {
      this.carService.getCarListFire().subscribe(
          data => {
              let users = data.map(
                  e => {
                      return {
                          id: e.payload.val().id,
                          // id: e.payload.key,
                          type: e.payload.val().type,
                          marque: e.payload.val().marque,
                          prix: e.payload.val().prix,
                          location: e.payload.val().location,
                          urlImage: e.payload.val().urlImage,
                          description: e.payload.val().description,
                          status: e.payload.val().status
                      };
                  }
              );
              this.carService.setAllCarClass(users);
          }
      );
      this.carService.cars$.subscribe(
          (cars) => {
              this.cars = cars;
              this.loading = false;
          }
      );
      this.userConnectedPage = this.userService.getUserConnected();
      this.userService.userConnected$.subscribe(
          (userConnected) => {
              this.userConnectedPage = userConnected;
          }
      );
  }
  setCarAvailable(car: Car) {
      car.status = 'available';
      let indexCar = this.carService.findIndexCar(car);
      this.carService.updateCarBase(indexCar.toString(), car).then();
  }

}
