import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarSoldedPage } from './car-solded.page';

describe('CarSoldedPage', () => {
  let component: CarSoldedPage;
  let fixture: ComponentFixture<CarSoldedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarSoldedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarSoldedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
