import {Component, OnInit } from '@angular/core';
import {MenuController } from '@ionic/angular';
import { AuthService } from '../../service/auth.service';
import { PopoverController } from '@ionic/angular';
import { PopoverCarListComponent } from '../../components/popover-car-list/popover-car-list.component';
import {UserService} from '../../service/user.service';
import {UserProfilModel} from '../../model/user/userProfil.model';
import {CarService} from '../../service/car.service';
import {Car} from '../../model/car/car.model';
import {Subscription} from 'rxjs';
// import { AngularFireStorage } from 'angularfire2/storage';

@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.page.html',
  styleUrls: ['./car-list.page.scss'],
})
export class CarListPage implements OnInit {
    cars: Car[];
    loading: boolean = true;
    isAuth: boolean;

    constructor(private authService: AuthService,
                public popoverController: PopoverController,
                public menuCtrl: MenuController,
                private userService: UserService,
                private carService: CarService) {}

    ngOnInit() {
        this.carService.getCarListFire().subscribe(
            data => {
                let users = data.map(
                    e => {
                        return {
                            id: e.payload.val().id,
                            // id: e.payload.key,
                            type: e.payload.val().type,
                            marque: e.payload.val().marque,
                            prix: e.payload.val().prix,
                            location: e.payload.val().location,
                            urlImage: e.payload.val().urlImage,
                            description: e.payload.val().description,
                            status: e.payload.val().status
                        };
                    }
                );
                this.carService.setAllCarClass(users);
            }
        );
        this.carService.cars$.subscribe(
            (cars) => {
                this.cars = cars;
                this.loading = false;
            }
        );
        this.menuCtrl.enable(this.authService.authenticated());
        this.isAuth = this.authService.authenticated();
    }
    getIsAuth(): boolean {
        return this.authService.authenticated();
    }
    getIsAuthAdmin(): boolean {
        if(this.authService.authenticated() && (this.userService.getUserConnected().typeUser === 'admin') ) {
            return true;
        } else {
            return false;
        }
    }
    async presentPopover(ev: any) {
        const popover = await this.popoverController.create({
            component: PopoverCarListComponent,
            event: ev,
            translucent: true
        });
        return await popover.present();
    }

}
