import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../service/auth.service';
import { NgForm } from '@angular/forms';
import {UserService} from '../../service/user.service';
import {MenuController, ToastController} from '@ionic/angular';
import {__await} from 'tslib';
import {UserProfilModel} from '../../model/user/userProfil.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  imageToUpload: string;
  file: File;
  sexe: string;
  RegisterError: string;
  urlImageDownload: string;
  noConn = 'A network error (such as timeout, interrupted connection or unreachable host) has occurred.';
  badLogin = 'The email address is badly formatted.';
  existLogin = 'The email address is already in use by another account.';
  loading = 'Verification...' ;
  passwordNotValid = 'Verifier votre mot de passe';
  // password
  passwordIcon = 'eye-off';
  passwordType = 'password';
  cpasswordIcon = 'eye-off';
  cpasswordType = 'password';
  showHidePwd() {
      this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
      this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }
  showHideCPwd() {
    this.cpasswordType = this.cpasswordType === 'text' ? 'password' : 'text';
    this.cpasswordIcon = this.cpasswordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  constructor(private router: Router,
              private authService: AuthService,
              private userService: UserService,
              public menuCtrl: MenuController,
              public toastController: ToastController) {
      this.menuCtrl.enable(this.authService.authenticated());
  }
  async presentToast() {
    const toast = await this.toastController.create({
        message: 'Vous ête inscrit',
        duration: 2000,
        buttons: [
            {
                icon: 'close',
            }
        ]
    });
    toast.present();
  }

  ngOnInit() {
  }

  setSexeValue(value){
    this.sexe = value.detail.valueOf().value;
  }

  onSubmitRegister(form: NgForm) {
      const nameUser = form.value['nameUser'];
      const firstNameUser = form.value['firstNameUser'];
      const email = form.value['loginUser'];
      const cinUser = form.value['cinUser'];
      const addressUser = form.value['addressUser'];
      const mdp = form.value['mdpUser'];
      const cmdp = form.value['cmdpUser'];

      let client: UserProfilModel =  {
          key: '',
          login: email,
          name: nameUser,
          firstName: firstNameUser,
          gender: this.sexe,
          cin: cinUser,
          location: addressUser,
          password: mdp,
          urlImage: '',
          typeUser: 'client'
      };
      if (mdp === cmdp) {
          this.RegisterError = this.loading;
          this.authService.signUp(client)
              .then(
                  () => {
                      this.userService.saveUser(this.file, client);
                      this.RegisterError = '';
                      form.reset();
                      this.presentToast();
                      this.router.navigate(['login']);
                  },
                  (error) => {
                      this.RegisterError = error.message;
                  }
              );
      } else {
          this.RegisterError = this.passwordNotValid;
      }
 }

  messageResponse(): string {
    if ( this.RegisterError === this.badLogin) {
        return 'Mauvais format de l\'email';
    } else if ( this.RegisterError === this.noConn) {
      return 'Il n\'y a pas de connexion. Veuillez connecter votre appareil';
    } else if ( this.RegisterError === this.existLogin ) {
      return 'Email déjà utiliser par un autre utilisateur';
    } else if ( this.RegisterError === this.passwordNotValid ) {
        return this.passwordNotValid;
    } else {
        return '' ;
    }
  }
  changeListener($event) {
      this.file = $event.target.files[0];
      if (this.file !== null) {
          let reader = new FileReader();
          reader.onload = (event: any) => {
              this.imageToUpload = event.target.result;
          };
          reader.readAsDataURL($event.target.files[0]);
      }
  }
}
