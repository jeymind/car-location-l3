import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FourZeroFourPage } from './four-zero-four.page';

describe('FourZeroFourPage', () => {
  let component: FourZeroFourPage;
  let fixture: ComponentFixture<FourZeroFourPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FourZeroFourPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FourZeroFourPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
