import { Component, OnInit } from '@angular/core';
import {MenuController} from '@ionic/angular';
import {AuthService} from '../../service/auth.service';

@Component({
  selector: 'app-four-zero-four',
  templateUrl: './four-zero-four.page.html',
  styleUrls: ['./four-zero-four.page.scss'],
})
export class FourZeroFourPage implements OnInit {

  constructor(private authService: AuthService,
              public menuCtrl: MenuController) {
      this.menuCtrl.enable(this.authService.authenticated());
  }

  ngOnInit() {
  }

}
