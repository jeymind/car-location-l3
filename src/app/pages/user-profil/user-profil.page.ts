import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../service/auth.service';
import {MenuController} from '@ionic/angular';
import {UserService} from '../../service/user.service';
import {UserProfilModel} from '../../model/user/userProfil.model';
import {NgForm} from '@angular/forms';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-user-profil',
  templateUrl: './user-profil.page.html',
  styleUrls: ['./user-profil.page.scss'],
})
export class UserProfilPage implements OnInit {
    userConnectedPage: UserProfilModel;
    action: boolean;
    genderText: string;
    file: File;
    percentage: Observable<number>;
    constructor(private authService: AuthService,
                public menuCtrl: MenuController,
                private userService: UserService) {}

    setEditAction() {
      this.action = true;
    }

    ngOnInit() {
        this.menuCtrl.enable(this.authService.authenticated());
        this.userConnectedPage = this.userService.getUserConnected();
        this.userService.userConnected$.subscribe(
            (userConnected) => {
                this.userConnectedPage = userConnected;
            }
        );
        this.action = false;
    }

    setSexeValue(value){
        this.genderText = value.detail.valueOf().value;
    }

    onSubmitUpdateUser(form: NgForm) {
        this.userConnectedPage.gender = this.genderText;
        this.userService.saveUser(this.file, this.userConnectedPage);
        this.userService.emitUserConnected();
        this.action = false;
    }
    changeListener($event) {
        this.file = $event.target.files[0];
        if (this.file !== null) {
            let reader = new FileReader();
            reader.onload = (event: any) => {
                this.userConnectedPage.urlImage = event.target.result;
            };
            reader.readAsDataURL($event.target.files[0]);
        }
    }
}
