import { Component, OnInit } from '@angular/core';
import { NgForm} from '@angular/forms';
import {UserProfilModel} from '../../model/user/userProfil.model';
import {Car} from '../../model/car/car.model';
import {CarService} from '../../service/car.service';
import {ToastController} from '@ionic/angular';
import {ControlService} from '../../service/control.service';

@Component({
  selector: 'app-car-add',
  templateUrl: './car-add.page.html',
  styleUrls: ['./car-add.page.scss'],
})
export class CarAddPage implements OnInit {
  file: File;
  imageToUpload: string;
  carsClass: Car[];
  constructor(private carService: CarService,
              private controlService: ControlService,
              private toastController: ToastController) { }
  ngOnInit() {
      this.carsClass = this.carService.getCarsClass();
      this.carService.cars$.subscribe(
          (cars) => {
              this.carsClass = cars;
          }
      );
  }
  onSubmitRegister(form: NgForm) {
      const typeCar = form.value['typeCar'];
      const marqueCar = form.value['marqueCar'];
      const prixCar = form.value['prixCar'];
      const locationCar = form.value['locationCar'];
      const descriptionCar = form.value['descriptionCar'];
      let numCar = this.controlService.generateIdObject(this.carsClass, 'V');
      let carObject: Car = {
        id: numCar,
        type: typeCar,
        marque: marqueCar,
        prix: prixCar,
        location: locationCar,
        urlImage: '',
        description: descriptionCar,
        status: 'available'
      };
      this.carService.saveStoreCars(this.file, carObject);
      this.imageToUpload = '../../../assets/img/logo.png';
      form.reset();
      this.presentToast();
  }
  async presentToast() {
      const toast = await this.toastController.create({
          message: 'Voiture enregistrer',
          duration: 2000,
          buttons: [
              {
                  icon: 'close',
              }
          ]
      });
      toast.present();
  }
  changeListener($event) {
      this.file = $event.target.files[0];
      if (this.file !== null) {
          let reader = new FileReader();
          reader.onload = (event: any) => {
              this.imageToUpload = event.target.result;
          };
          reader.readAsDataURL($event.target.files[0]);
      }
  }


}
