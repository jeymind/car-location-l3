import {Component, ViewChild, ElementRef, OnInit} from '@angular/core';
import { AuthService } from '../../service/auth.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import {MenuController} from '@ionic/angular';

declare var google: any;

@Component({
  selector: 'app-home',
  templateUrl: 'accueil.page.html',
  styleUrls: ['accueil.page.scss'],
})
export class AccueilPage implements OnInit {
 //   @ViewChild('map') mapElement: ElementRef;
 //   map: any;
 //   address: string;
    constructor(private geolocation: Geolocation,
                private authService: AuthService,
                public menuCtrl: MenuController) {
        this.menuCtrl.enable(this.authService.authenticated());
    }



    ngOnInit(): void {
//        this.loadMap();
    }

    /*
    loadMap() {
        this.geolocation.getCurrentPosition().then((res) => {
            // let location = new google.maps.LatLng(res.coords.latitude,res.coords.longitude);
             let location = new google.maps.LatLng(-21.439204, 47.100378);
            let mapOptions = {
                center: location,
                zoom: 18,
                // mapTypeId: 'satellite'
            }

            this.getAddressFromCoords(res.coords.latitude, res.coords.longitude);

            this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

            this.map.addListener('tilesloaded', () => {
                console.log('accuracy',this.map);
                this.getAddressFromCoords(this.map.center.lat(), this.map.center.lng())
            });

            }).catch((error) => {
                console.log('Error getting location', error);
            });
    }

    getAddressFromCoords( lattitude, longitude) {
        console.log("getAddressFromCoords " + lattitude+ " " + longitude);
    }*/
}
