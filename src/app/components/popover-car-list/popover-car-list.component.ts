import { Component, OnInit } from '@angular/core';
import {PopoverController} from '@ionic/angular';

@Component({
  selector: 'app-popover-car-list',
  templateUrl: './popover-car-list.component.html',
  styleUrls: ['./popover-car-list.component.scss'],
})
export class PopoverCarListComponent implements OnInit {

  constructor(public popoverController: PopoverController) { }

  ngOnInit() {}
  closePopover(){
    this.popoverController.dismiss();
  }
}
